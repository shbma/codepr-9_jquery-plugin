//Version: 1
//Author: Michael Botov

(function($){
	'use strict';
	var Signup = function(element,settings){
		var t = this;

		t.element = element;

		t.defaults = {
			'button':{
				'text':'Записаться на собеседование',				
			},			
			'tabs':{
				'header': 'Что бы записаться на собеседование,<br>выберите один из вариантов:',
				'items': [
					{
						'title':'Facebook',
						'anchor': 'fb',
						'isActive': true,
						'actions': [
							{
								'title':'Поставьте лайк на странице школы',
								'title_display':'inline',
								'code':
									'<div class="fb-like fb_iframe_widget" data-href="https://www.facebook.com/codepr" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false" fb-xfbml-state="rendered" '
										+'fb-iframe-plugin-query="action=like&amp;app_id=704382262995595&amp;container_width=0&amp;href=https%3A%2F%2Fwww.facebook.com%2Fcodepr&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=false&amp;show_faces=false">'
										+'<span style="vertical-align: bottom; width: 81px; height: 20px;">'
										+'	<iframe name="f1c039c98" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="http://www.facebook.com/v2.4/plugins/like.php?action=like&amp;app_id=704382262995595&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2Fjb3BUxkAISL.js%3Fversion%3D41%23cb%3Df3bc63be48%26domain%3Dcodepr.ru%26origin%3Dhttp%253A%252F%252Fcodepr.ru%252Ff1fd08334c%26relation%3Dparent.parent&amp;container_width=0&amp;href=https%3A%2F%2Fwww.facebook.com%2Fcodepr&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=false&amp;show_faces=false" class="" style="border: none; visibility: visible; width: 81px; height: 20px;"></iframe>'
										+'</span>'
	                            	+'</div>',
	                            'warning':
	                            	'Нажмите на кнопку "Like" в первом пункте.<br>'+ 
	                            	'Если лайк уже есть, нажмите на кнопку "Like" дважды.',
							},
							{
								'title':'Поделитесь ссылкой на сайт школы',
								'title_display':'inline',
								'code':
									'<a href="#" class="code-share-fb-link">'
                            			+'<img src="/img/share-fb.png" class="code-share-fb-img">'
                            		+'</a>',
                            	'warning':
                            		'Нажмите на кнопку "Share" во втором пункте<br>'
                    				+'и поделитесь ссылкой на своей странице.',
							},							
						],
						'subText': 'Не удаляйте запись со своей страницы до начала обучения.',
					},
					{
						'title':'ВКонтакте',
						'anchor': 'vk',
						'isActive': false,
						'actions': [
							{
								'title':'Подпишитесь на страницу школы:',
								'title_display':'inline',
								'code':
									'<div id="vk_subscribe" style="height: 22px; width: 100%; background: none;">'
										+'<iframe name="fXDadf32" frameborder="0" src="http://vk.com/widget_subscribe.php?app=5018418&amp;width=100%&amp;_ver=1&amp;oid=-82352344&amp;mode=1&amp;soft=1&amp;url=http%3A%2F%2Fcodepr.ru%2Fschool%2Fhtml5&amp;referrer=http%3A%2F%2Fcodepr.ru%2F&amp;title=%D0%97%D0%B0%D0%BF%D0%B8%D1%81%D0%B0%D1%82%D1%8C%D1%81%D1%8F%20%D0%BD%D0%B0%20%D0%B2%D0%B5%D0%B1%D0%B8%D0%BD%D0%B0%D1%80&amp;1508149d1b5" width="100%" height="22" scrolling="no" id="vkwidget1" style="overflow: hidden;"></iframe>'
									+'</div>'                            
                            		+'<script type="text/javascript">'
                                		+'VK.Widgets.Subscribe("vk_subscribe", {mode: 1, soft: 1}, -82352344);'
                            		+'</script>',
                            	'warning':
                            		'Нажмите на кнопку "Подписаться" в первом пункте.<br>'
                    				+'Если вы уже подписаны, нажмите "Отмена" и "Подписаться".',
							},
							{
								'title':'Нажмите "Мне нравится" и "Рассказать друзьям":',
								'title_display':'inline',
								'code':
									'<div id="vk_like" style="height: 20px; width: 180px; position: relative; clear: both; background: none;">'
										+'<iframe name="fXDff738" frameborder="0" src="http://vk.com/widget_like.php?app=5018418&amp;width=100%&amp;_ver=1&amp;page=0&amp;url=http%3A%2F%2Fcodepr.ru%2F%3Fgo&amp;type=button&amp;verb=0&amp;color=&amp;title=%D0%97%D0%B0%D0%BF%D0%B8%D1%81%D0%B0%D1%82%D1%8C%D1%81%D1%8F%20%D0%BD%D0%B0%20%D0%B2%D0%B5%D0%B1%D0%B8%D0%BD%D0%B0%D1%80&amp;description=%D0%98%D0%BD%D1%82%D0%B5%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D0%B2%D0%BD%D0%BE%D0%B5%20%D0%BE%D0%B1%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D0%B5%20%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D1%8E%20%D0%B2%D0%B5%D0%B1-%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%BE%D0%B2%20%D0%B8%20%D0%BC%D0%BE%D0%B1%D0%B8%D0%BB%D1%8C%D0%BD%D1%8B%D1%85%20%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B9.&amp;image=http%3A%2F%2Fcodepr.ru%2Fimg%2Fcode-130.png&amp;text=%D0%AF%20%D0%B8%D0%B4%D1%83%20%D1%83%D1%87%D0%B8%D1%82%D1%8C%D1%81%D1%8F%20%D0%B2%20%D1%88%D0%BA%D0%BE%D0%BB%D1%83%20%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B8%20CODE%20production&amp;h=20&amp;height=20&amp;referrer=http%3A%2F%2Fcodepr.ru%2F&amp;1508149d1b9" width="100%" height="20" scrolling="no" id="vkwidget2" style="overflow: hidden; height: 20px; width: 180px; z-index: 150;"></iframe>'
									+'</div>'
		                            +'<script type="text/javascript">'
		                                +'VK.Widgets.Like("vk_like", {type: "button", height: 20, text: "Я иду учиться в школу разработки CODE production", pageUrl: "http://codepr.ru/?go", pageTitle: "Записаться на вебинар", pageDescription: "Интерактивное обучение созданию веб-проектов и мобильных приложений.", pageImage: "http://codepr.ru/img/code-130.png"});'
		                            +'</script>',
		                        'warning':
		                        	'Во втором пункте нажмите "Мне нравится"<br>'
                    				+'и "Рассказать друзьям".',
							},							
						],
						'subText': 'Не удаляйте запись со своей страницы до начала обучения.',
					},
					{
						'title':'Деньги',
						'anchor': 'ya',
						'isActive': false,
						'actions': [
							{
								'title':'Просто оплатите 300 <i class="fa fa-rub code-sm"></i> за собеседование:',
								'title_display':'inline',
								'code':
									'<iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/embed/small.xml?account=41001465916272&amp;quickpay=small&amp;yamoney-payment-type=on&amp;button-text=01&amp;button-size=s&amp;button-color=orange&amp;targets=%D0%9E%D0%BF%D0%BB%D0%B0%D1%82%D0%B0+%D0%B7%D0%B0+%D0%BF%D1%80%D0%BE%D0%B1%D0%BD%D1%8B%D0%B9+%D1%83%D1%80%D0%BE%D0%BA&amp;default-sum=300&amp;fio=on&amp;mail=on&amp;successURL=http%3A%2F%2Fcodepr.ru" width="137" height="31"></iframe>',
								'warning':'',
							},														
						],
						'subText': '',
					}
				]
			}, //end tabs
			'email':{
				'dataParam':'',
				'placeholder':'Электронная почта'
			},
			'want_buttom':{
				'dataPage':"html5",
				'dataGoal':"GO_COURSE_5",
				'text':'Записаться',
			},			
		};
		t.settings = $.extend({}, t.defaults, settings);
	
	}		

	Signup.prototype.makeMarkup = function(){
		var t = this;
		
		$('<button class="code-lesson-button-access">'+t.settings.button.text+'</button>').appendTo(t.element);
		//контейнер
		$('<div class="code-lesson-container"></div>').appendTo(t.element); 
		//контейнер для вкладок
		var cl_access_o = $(t.element).find('.code-lesson-container').append('<div class="code-lesson-access"></div>');
			//общий заголовок
			cl_access_o.append('<strong>'+t.settings.tabs.header+'</strong>');		
			//заголовки вкладок
			var cl_variants_o = cl_access_o.append('<div class="code-lesson-variants"></div>');
			var items = t.settings.tabs.items;
			var raw = '';
			for(var i in items){				
				raw +='<a href="#'+items[i].anchor+'" class="code-activate-'+items[i].anchor+' code-active">'+items[i].title+'</a>	<span>&nbsp;</span>';
			}
			cl_variants_o.html(raw);

			//вкладки с действиями
			for(var i in items){
				var suffix = items[i].anchor;	
				var var_class = 'code-lesson-variant-'+suffix;	//классс для вкладки		
				var cl_variant_o_curr = cl_access_o
					.append('<div class="'+var_class+'"></div>');
				if (!items[i].isActive){ //если не активная вкладка - укажем это классом
					cl_variant_o_curr.addClass('code-lesson-hide');
				}
				var cl_variant_o_curr_actions = cl_variant_o_curr 
					.append('<div class="code-lesson-actions"></div>'); //контейнер для действий 
				//рисуем сами действия
				for(var a in items[i].actions){
					var action = items[i].actions[a]; //текущее действие
					cl_variant_o_curr_actions
						.append( //код действия
							'<div class="code-lesson-action-' + suffix + ' code-lesson-action-' + suffix + '-' + a + '">'
	                            +'<span class="code-check">&nbsp;</span> '
	                            +'<span class="code-strike">' + a + '. ' + action.title + '</span> — '
	                            	 + action.code +
	                        '</div>'
							);
					cl_variant_o_curr_actions
						.append( //подпись под действиями
							'<div class="code-small">'+items[i].subText+'</div>'
						)
				}
			}

			//Эл.почта с кнопкой
			cl_access_o.append(
				'<div class="code-lesson-form code-waiting">'
                    +'<input type="text" id="code-student" name="code-student" data-param="'+t.settings.email.dataParam+'" placeholder="'+t.settings.email.placeholder+'">'
                    +'<button id="code-student-go" data-page="'+t.settings.want_buttom.dataPage+'" data-goal="'+t.settings.want_buttom.dataGoal+'">'+t.settings.want_buttom.text+'</button>'
                +'</div>'
				);
	
			//Предупреждения

			for(var i in items){
				var suffix = items[i].anchor;	

				for(var a in items[i].actions){
					var action = items[i].actions[a]; //текущее действие
					
					$(t.element).find('code-lesson-access').append(
						'<div class="code-lesson-warning code-lesson-'+suffix+a+'-warning">'+action.warning+'</div>'
						);
				}
				
			}

		//контейнер для сообщений
		$(t.element).find('.code-lesson-container').append('<div class="code-lesson-access-success hide"></div>');
	}

	$.fn.signup = function(options){
		//this - объект jQuery для которого был вызван плагин
		var t = this;		

		var max = 0;

		t.each(function(){			
			this.signup = new Signup(this,options);
			this.signup.makeMarkup();

			//max = Math.max(max, $(this).height());
			//$(this).css('font-size',settings['font-size']);
			console.log(this);
		})

		return t;		
		
	}


/*
    $.fn.slick = function() {
        var _ = this,
            opt = arguments[0],
            args = Array.prototype.slice.call(arguments, 1),
            l = _.length,
            i = 0,
            ret;
        for (i; i < l; i++) {
            if (typeof opt == 'object' || typeof opt == 'undefined')
                _[i].slick = new Slick(_[i], opt);
            else
                ret = _[i].slick[opt].apply(_[i].slick, args);
            if (typeof ret != 'undefined') return ret;
        }
        return _;
    };
*/


})(jQuery);

